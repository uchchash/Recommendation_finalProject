-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2017 at 02:53 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recommendation`
--

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `p_id` int(100) NOT NULL,
  `place` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`p_id`, `place`) VALUES
(1, 'Mirpur'),
(2, 'Dhanmondi'),
(3, 'Uttara'),
(4, 'Bashundhara'),
(5, 'Gulshan'),
(6, 'Malibag');

-- --------------------------------------------------------

--
-- Table structure for table `real_estate`
--

CREATE TABLE `real_estate` (
  `rs_id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(100) NOT NULL,
  `lattitude` float NOT NULL,
  `longitude` float NOT NULL,
  `p_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_estate`
--

INSERT INTO `real_estate` (`rs_id`, `name`, `price`, `lattitude`, `longitude`, `p_id`, `image`) VALUES
(1, '2048 sq-ft.(Type-A), 3 Bedroom Apartment for Sale in Navana City Within Urban Dale (Block-1)', 0, 23.805, 90.3835, 1, 'd1.jpg'),
(2, 'Apartment for Sale in Acme Casa Hermanas, Extended Pallabi', 0, 23.8244, 90.3617, 1, 'd2.jpg'),
(3, 'Apartment for Sale in Navana Probani Ridgedale (Block-1)', 0, 23.8184, 90.3742, 1, 'd3.jpg'),
(4, 'Apartment for Sale in IMAGINE EASTWOOD', 18500000, 23.8138, 90.4277, 4, 'd4.jpg'),
(5, 'Apartment for Sale in Bijoy Rakeen City', 0, 23.8045, 90.3816, 1, 'd5.jpg'),
(6, 'Apartment for Sale in Uttara', 0, 23.863, 90.4018, 3, 'd6.jpg'),
(7, 'Apartment for Sale in Landmark JKH Complex', 0, 23.7965, 90.4128, 5, 'building.jpg'),
(8, 'Apartment for Sale in Bijoy Rakeen City', 0, 23.8045, 90.3816, 1, 'blue.jpg'),
(9, 'apartment for sale in Sufia South Point', 9462000, 23.8216, 90.4311, 5, 'bed.jpg'),
(10, 'Apartment for sale in Priyoprangon Bashantika', 0, 23.757, 90.4643, 6, 'sd.jpg'),
(11, 'Apartment for Sale in Navana Probani Ridgedale (Block-5)', 0, 23.8184, 90.3742, 1, 'din.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(100) NOT NULL,
  `rs_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `hospitals` double NOT NULL,
  `environmental_surroundings` double NOT NULL,
  `roads` double NOT NULL,
  `electricity` double NOT NULL,
  `gas` double NOT NULL,
  `departmental_store` double NOT NULL,
  `school_colleges` double NOT NULL,
  `super_market` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `rs_id`, `user_id`, `hospitals`, `environmental_surroundings`, `roads`, `electricity`, `gas`, `departmental_store`, `school_colleges`, `super_market`) VALUES
(1, 3, 3, 4, 5, 3, 2, 4, 3, 4, 5),
(2, 2, 2, 5, 1, 3, 4, 5, 3, 2, 1),
(3, 9, 5, 3, 3, 5, 1, 4, 2, 4, 4),
(4, 6, 4, 2, 4, 2, 3, 3, 5, 3, 5),
(5, 8, 3, 3, 5, 5, 4, 4, 2, 3, 2),
(6, 11, 4, 4, 2, 3, 3, 3, 4, 5, 5),
(7, 5, 3, 4, 4, 4, 5, 3, 3, 2, 5),
(8, 10, 2, 3, 3, 2, 0, 4, 2, 1, 3),
(9, 4, 2, 3, 5, 1, 2, 4, 3, 5, 2),
(10, 7, 5, 3, 3, 4, 3, 2, 6, 4, 2),
(11, 10, 2, 3, 4, 2, 5, 5, 5, 4, 3),
(12, 11, 2, 3, 5, 3, 3, 4, 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rs_details`
--

CREATE TABLE `rs_details` (
  `id` int(100) NOT NULL,
  `rs_id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(100) NOT NULL,
  `size_sqft` int(100) NOT NULL,
  `room` int(100) NOT NULL,
  `bedroom` int(50) NOT NULL,
  `bath` int(50) NOT NULL,
  `floor` int(50) NOT NULL,
  `lift` varchar(50) NOT NULL,
  `generator` varchar(50) NOT NULL,
  `fire_exit` varchar(50) NOT NULL,
  `gas_connection` varchar(50) NOT NULL,
  `wasa_connection` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_details`
--

INSERT INTO `rs_details` (`id`, `rs_id`, `name`, `price`, `size_sqft`, `room`, `bedroom`, `bath`, `floor`, `lift`, `generator`, `fire_exit`, `gas_connection`, `wasa_connection`, `image`) VALUES
(1, 1, 'Apartment for Sale in Navana City Within Urban Dale (Block-1)', 0, 2048, 6, 3, 3, 3, 'Yes', 'yes', 'yes', 'yes', 'yes', 'd1.jpg'),
(2, 2, 'Apartment for Sale in Acme Casa Hermanas, Extended Pallabi', 0, 1150, 5, 3, 3, 5, 'yes', 'yes', 'no', 'yes', 'yes', 'd2.jpg'),
(3, 3, 'Apartment for Sale in Navana Probani Ridgedale (Block-1)', 0, 2246, 6, 3, 3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', 'd3.jpg'),
(4, 4, 'Apartment for Sale in IMAGINE EASTWOOD', 18500000, 2360, 6, 3, 4, 4, 'yes', 'yes', 'no', 'yes', 'yes', 'd4.jpg'),
(5, 5, 'Apartment for Sale in Bijoy Rakeen City', 0, 1872, 5, 3, 3, 15, 'yes', 'yes', 'yes', 'yes', 'yes', 'd5.jpg'),
(6, 6, 'Apartment for Sale in Uttara', 0, 1900, 5, 3, 3, 2, 'yes', 'yes', 'yes', 'yes', 'yes', 'd6.jpg'),
(7, 7, 'Apartment for Sale in Landmark JKH Complex', 0, 3844, 6, 4, 4, 1, 'yes', 'yes', 'yes', 'yes', 'yes', 'building.jpg'),
(8, 8, 'Apartment for Sale in Bijoy Rakeen City', 0, 1553, 5, 3, 3, 15, 'yes', 'yes', 'yes', 'yes', 'no', 'blue.jpg'),
(9, 9, 'apartment for sale in Sufia South Point', 9462000, 1450, 5, 3, 4, 6, 'yes', 'yes', 'no', 'no', 'yes', 'bed.jpg'),
(10, 10, 'Apartment for sale in Priyoprangon Bashantika', 0, 1436, 5, 3, 3, 3, 'yes', 'yes', 'yes', 'yes', 'yes', 'sd.jpg'),
(11, 11, 'Apartment for Sale in Navana Probani Ridgedale (Block-5)', 0, 1411, 5, 3, 3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', 'din.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_verified` varchar(10) NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `is_verified`) VALUES
(2, 'uchchash', '123456789', 'shrabon56@gmail.com', 'Yes'),
(3, 'shemonti', '121234345656', 'shemonti@gmail.com', 'Yes'),
(4, 'samiran', '12345678', 'samiran@icddrb.org', 'Yes'),
(5, 'antor', '12123456', 'antor@yahoo.com', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE `user_preferences` (
  `id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `hospitals` double NOT NULL,
  `environmental_surroundings` double NOT NULL,
  `roads` double NOT NULL,
  `electricity` double NOT NULL,
  `gas` double NOT NULL,
  `departmental_store` double NOT NULL,
  `school_colleges` double NOT NULL,
  `super_market` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_preferences`
--

INSERT INTO `user_preferences` (`id`, `user_id`, `hospitals`, `environmental_surroundings`, `roads`, `electricity`, `gas`, `departmental_store`, `school_colleges`, `super_market`) VALUES
(1, 2, 3.4, 3.6, 2.2, 2.8, 4.4, 3.2, 2.4, 1.8),
(2, 5, 3, 3, 4.5, 2, 3, 4, 4, 3),
(3, 3, 3.67, 4.67, 4, 3.67, 3.67, 2.67, 3, 4),
(4, 4, 3, 3, 2.5, 3, 3, 4.5, 4, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `real_estate`
--
ALTER TABLE `real_estate`
  ADD PRIMARY KEY (`rs_id`),
  ADD KEY `Index` (`p_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `rs_id` (`rs_id`);

--
-- Indexes for table `rs_details`
--
ALTER TABLE `rs_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rs_id` (`rs_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
  MODIFY `p_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `real_estate`
--
ALTER TABLE `real_estate`
  MODIFY `rs_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `rs_details`
--
ALTER TABLE `rs_details`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_preferences`
--
ALTER TABLE `user_preferences`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `real_estate`
--
ALTER TABLE `real_estate`
  ADD CONSTRAINT `real_estate_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `place` (`p_id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`rs_id`) REFERENCES `real_estate` (`rs_id`);

--
-- Constraints for table `rs_details`
--
ALTER TABLE `rs_details`
  ADD CONSTRAINT `rs_details_ibfk_1` FOREIGN KEY (`rs_id`) REFERENCES `real_estate` (`rs_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
