<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\recommendation\recommendation;
$auth= new recommendation();
$auth->setData($_SESSION);
$all=$auth->view();
foreach($all as $alll) {
    $value = $alll->id;
}
$show=$auth->select($value);
$realestate=$auth->rs();
$i1=array();
$i2=array();
$i3=array();
$i4=array();
$i5=array();
$i6=array();
$i7=array();
$i8=array();
$j1=array();
$j2=array();
$j3=array();
$j4=array();
$j5=array();
$j6=array();
$j7=array();
$j8=array();
foreach ($realestate as $place)
{
    $i1[]=$place->hospitals;
    $i2[]=$place->environmental_surroundings;
    $i3[]=$place->roads;
    $i4[]=$place->electricity;
    $i5[]=$place->gas;
    $i6[]=$place->departmental_store;
    $i7[]=$place->school_colleges;
    $i8[]=$place->super_market;

}
foreach ($show as $user)
{
    $j1[]=$user->hospitals;
    $j2[]=$user->environmental_surroundings;
    $j3[]=$user->roads;
    $j4[]=$user->electricity;
    $j5[]=$user->gas;
    $j6[]=$user->departmental_store;
    $j7[]=$user->school_colleges;
    $j8[]=$user->super_market;

}
$similarity=array();

for($a=0;$a<sizeof($j1);$a++)
{
    for($b=0;$b<sizeof($i1);$b++)
    {
        $numerator=($j1[$a]*$i1[$b])+($j2[$a]*$i2[$b])+($j3[$a]*$i3[$b])+($j4[$a]*$i4[$b])+($j5[$a]*$i5[$b])+($j6[$a]*$i6[$b])+($j7[$a]*$i7[$b])+($j8[$a]*$i8[$b]);
        $denominator1=sqrt(pow($j1[$a],2)+pow($j2[$a],2)+pow($j3[$a],2)+pow($j4[$a],2)+pow($j5[$a],2)+pow($j6[$a],2)+pow($j7[$a],2)+pow($j8[$a],2));
        $denominator2=sqrt(pow($i1[$b],2)+pow($i2[$b],2)+pow($i3[$b],2)+pow($i4[$b],2)+pow($i5[$b],2)+pow($i6[$b],2)+pow($i7[$b],2)+pow($i8[$b],2));

        $total_denominator=$denominator1*$denominator2;
        $similarity[]=$numerator/$total_denominator;

    }

}
arsort($similarity);

$h=implode(', ', array_keys($similarity));

$ha=explode(" ",$h);

$send_rsid=array();
foreach ($ha as $present_value)
{
$checking=$auth->fetching_details($present_value);

    foreach ($checking as $item) {
        $send_rsid[]=$item->rs_id;

    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Template</title>
    <link rel="stylesheet" href="../../Resources/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="style.css"> -->

    <script src="../../Resources/bootstrap/js/jquery-3.2.0.min.js"></script>
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">


</head>
<body>
<div>
    <div class="header">
        <div class="container">
            <b>
                R E A L - E S T A T O R
            </b>


        </div>

    </div>

    <div class="middle">

        <nav class="navbar navbar-inverse">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <form class="navbar-form navbar-right" action="#" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="Name">
                        </div>
                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-left">
                        <li ><a href="#" class="hi">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Buy</a></li>
                        <li><a href="#">Rent</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Area <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Dhaka</a></li>
                                <li><a href="#">Chittagong</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <h1 style="color: #5bc0de;text-align: center">Recommendation for you</h1>

<?php
$array_save=array();
$array_img=array();
$array_id=array();

        foreach ($send_rsid as $get_rsid)
        {
        $rs_name=$auth->fetching_rsname($get_rsid);

foreach ($rs_name as $deta)
{
    $array_save[]=$deta->name;
    $array_img[]=$deta->image;
    $array_id[]=$deta->rs_id;
}

}

//
//$h=implode(', ', $array_id);
//
//$ha=explode(" ",$h);
//var_dump($ha);

        for($d=0;$d<sizeof($array_save);$d++)
        {
            $e=$d+1;
            $f=$d+2;
            echo"
            <div class='container'style='color: #5bc0de'>
                    <div class='col-lg-4'>
                    <img src='../../Resources/images/$array_img[$d]' style='width: 100px;height: 100px;'><br>
                    <a href='view.php?id=$array_id[$d];' class='btn btn-info'>$array_save[$d]</a>
                    </div>

                    <div class='col-lg-4'>
                    <img src='../../Resources/images/$array_img[$e]' style='width: 100px;height: 100px;'><br> 
                    <a href='view.php?id=$array_id[$e];' class='btn btn-info'>$array_save[$e]</a>
                    <br>
                    
                    </div>

                    <div class='col-lg-4'>
                    <img src='../../Resources/images/$array_img[$f]' style='width: 100px;height: 100px;'><br>
                    <a href='view.php?id=$array_id[$f];' class='btn btn-info'>$array_save[$f]</a></div>
                    </div>
                    ";
            $d=$d+2;
        }


?>

        <div class="slider">
            <div class="container">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="../../Resources/images/dhaka.jpg" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="../../Resources/images/banglow.jpg" alt="Second slide">
                        </div>
                        <div class="item">
                            <img src="../../Resources/images/moru.jpg" alt="Third slide">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="diffitem" >
            <div class="container">
                <div class="col-md-4" style="text-align: center">
                    <img src="../../Resources/images/28.jpg" style="border-radius: 100%">
                    <h3>Latest</h3>
                    <p style="text-align: justify">Looking for your dream home? Well, look no further, because Lamudi has what you are looking for! With our busy lives, it is key to have a reliable source when looking to invest in property, which is where our platform comes into play. Lamudi is the country's best online property finder, providing customers with a wide selection of apartments, houses, plots of land and commercial properties for sale and for rent. Individual sellers can also sell their properties in a secure way. Lamudi allows you to choose the desired accommodation and commercial property via trusted real estate agents on a guaranteed secure platform. Go ahead and try it now!</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="../../Resources/images/bed.jpg" style="border-radius: 100%">
                    <h3>High quality Properties</h3>
                    <p style="text-align: justify">Looking for your dream home? Well, look no further, because Lamudi has what you are looking for! With our busy lives, it is key to have a reliable source when looking to invest in property, which is where our platform comes into play. Lamudi is the country's best online property finder, providing customers with a wide selection of apartments, houses, plots of land and commercial properties for sale and for rent. Individual sellers can also sell their properties in a secure way. Lamudi allows you to choose the desired accommodation and commercial property via trusted real estate agents on a guaranteed secure platform. Go ahead and try it now!</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="../../Resources/images/building.jpg" style="border-radius: 100%">
                    <h3>Recommendation</h3>
                    <p style="text-align: justify">Looking for your dream home? Well, look no further, because Lamudi has what you are looking for! With our busy lives, it is key to have a reliable source when looking to invest in property, which is where our platform comes into play. Lamudi is the country's best online property finder, providing customers with a wide selection of apartments, houses, plots of land and commercial properties for sale and for rent. Individual sellers can also sell their properties in a secure way. Lamudi allows you to choose the desired accommodation and commercial property via trusted real estate agents on a guaranteed secure platform. Go ahead and try it now!</p>
                </div>
            </div>
        </div>


        <div class="formforus">
            <div class="container">
                <div class="form-group">
                    <form class="form-horizontal well ">
                        <fieldset>
                            <legend class="text-center">Help us to recommend you</legend>
                            <div class="form-group row">
                                <label for="inputfstnm" class="col-sm-2 col-form-label">Firstname</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputfstnm" placeholder="Firstname">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputlasname" class="col-sm-2 col-form-label">Lastname</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputlasname" placeholder="Lastname">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputaddress" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputaddress" placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputcity" class="col-sm-2 col-form-label">city</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputcity" placeholder=city>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputstate" class="col-sm-2 col-form-label">state</label>
                                <div class="col-sm-10">
                                    <select name="state" class="form-control selectpicker" id="inputstate">
                                        <option value=" " >Please select your state</option>
                                        <option>Dhaka</option>
                                        <option>Chittagong</option>
                                        <option >Sylhet</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputzip" class="col-sm-2 col-form-label">Zip</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputzip" placeholder="Zip">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 "></label>

                                <div class="col-lg-4 position">
                                    <button type="submit" class="btn-default" value="Submit" style="height: 30px;width: 100px;text-align: center">
                                        Submit
                                    </button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


    <div class="footer">
        <div class="container">
            <div class="col-lg-4">
                <h3>About</h3>
                <hr>
                <br>
                <p>About us</p><br>
                <p>Term of trade</p><br>
                <p>Privacy policy</p><br>
                <p>Copyright</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Apartment for rent</h3>
                <hr>
                <br>
                <p>Dhaka</p><br>
                <p>Chittagong</p><br>
                <p>Gulshan</p><br>
                <p>Dhanmondi</p><br>
                <p>Banani</p><br>
                <p>Uttara</p><br>
                <p>Agrabad</p><br>
                <p>Nasirabad</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Contact with us</h3>
                <hr>
                <a href="www.facebook.com"><img src="../../Resources/images/facebook.png"></a>
                <a href="www.twitter.com"><img src="../../Resources/images/twitter.png"></a>
                <a href="www.google.com"><img src="../../Resources/images/google_plus.png"></a>
                <a href="www.linkedin.com"><img src="../../Resources/images/linkedin.png"></a>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>


<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>

</body>
</html>