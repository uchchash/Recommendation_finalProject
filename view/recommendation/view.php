<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\recommendation\recommendation;
$auth= new recommendation();
$auth->setData($_GET);
$oneData  =  $auth->show();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Template</title>
    <link rel="stylesheet" href="../../Resources/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="style.css"> -->

    <script src="../../Resources/bootstrap/js/jquery-3.2.0.min.js"></script>
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">


</head>
<body>
<div>
    <div class="header">
        <div class="container">
            <b>
                R E A L - E S T A T O R
            </b>


        </div>

    </div>

    <div class="middle">

        <nav class="navbar navbar-inverse">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <form class="navbar-form navbar-right" action="#" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="Name">
                        </div>
                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-left">
                        <li ><a href="#" class="hi">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Buy</a></li>
                        <li><a href="#">Rent</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Area <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Dhaka</a></li>
                                <li><a href="#">Chittagong</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>



        <?php

        if($oneData->price==0) {
            $oneData->price = "Contact for price";
            $print = $oneData->price;
        }
            else
            $print="BDT ".$oneData->price;
       echo"
       <div class='col-lg-12 container' style='color:#5bc0de;margin-bottom: 50px '>
       <div class='col-lg-4 container' style=' text-align: right'>
       <img src='../../Resources/images/$oneData->image' style='width: 300px;height: 300px;'><br>
       </div>
        
       <div class='col-lg-8'>
       <h2>$oneData->name</h2><br>
       <h4>$oneData->size_sqft sq-ft</h4><br>
       <h4>$print</h4><br>
       
       <b>$oneData->bedroom Bedrooms &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
       <b>$oneData->bath Baths</b><br><br><br>
       </div>
       </div>
     <div class='container' style='color:#5bc0de;padding-left: 75px '>
     <h3>Apartment Details-----------------------------------------</h3><br>
     <div class='col-lg-3'>
     Size in sq-ft: <br><br>
     Floor: <br><br>
     Room: <br><br>
     Bedromm: <br><br>
     Bath: <br><br>
     </div>
     
     <div class='col-lg-3'>
      $oneData->size_sqft<br><br>
      $oneData->floor<br><br>
      $oneData->room<br><br>
      $oneData->bedroom<br><br>
      $oneData->bath <br><br>
      </div>
      
      <div class='col-lg-3'>
     Lift: <br><br>
     Generator: <br><br>
     Fire-exit: <br><br>
     Gas-connection: <br><br>
     WASA-connection: <br><br>
      </div>
           
      <div class='col-lg-3'>
     $oneData->lift<br><br>
     $oneData->generator<br><br>
      $oneData->fire_exit<br><br>
     $oneData->gas_connection<br><br>
      $oneData->wasa_connection<br><br>
     
       </div>
           
     
     </div><br><br>
     <div class='container'>
     <div class='col-lg-4'></div>
     <div class='col-lg-4'>
      <a href='rate.php?id=$oneData->rs_id;' class='btn btn-info' style='margin-left: 50px'>Rate The Apartment</a></div>
</div>
     <div class='col-lg-4'></div>
</div>
       
       
       ";


        ?>




    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


    <div class="footer">
        <div class="container">
            <div class="col-lg-4">
                <h3>About</h3>
                <hr>
                <br>
                <p>About us</p><br>
                <p>Term of trade</p><br>
                <p>Privacy policy</p><br>
                <p>Copyright</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Apartment for rent</h3>
                <hr>
                <br>
                <p>Dhaka</p><br>
                <p>Chittagong</p><br>
                <p>Gulshan</p><br>
                <p>Dhanmondi</p><br>
                <p>Banani</p><br>
                <p>Uttara</p><br>
                <p>Agrabad</p><br>
                <p>Nasirabad</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Contact with us</h3>
                <hr>
                <a href="www.facebook.com"><img src="../../Resources/images/facebook.png"></a>
                <a href="www.twitter.com"><img src="../../Resources/images/twitter.png"></a>
                <a href="www.google.com"><img src="../../Resources/images/google_plus.png"></a>
                <a href="www.linkedin.com"><img src="../../Resources/images/linkedin.png"></a>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>


<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>

</body>
</html>