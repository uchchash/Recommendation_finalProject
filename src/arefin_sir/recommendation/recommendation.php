<?php
/**
 * Created by PhpStorm.
 * User: UB
 * Date: 9/25/2017
 * Time: 12:17 PM
 */

namespace App\recommendation;


use App\Model\Database;

use PDO;


class recommendation extends Database
{


    public $name, $email, $pass;

    public function setData($postArray)
    {

        if (array_key_exists("Name", $postArray))
            $this->name = $postArray['Name'];

        if (array_key_exists("password", $postArray))
            $this->pass = $postArray['password'];

        if (array_key_exists("Email", $postArray))
            $this->email = $postArray['Email'];

        if (array_key_exists("id", $postArray))
            $this->id = $postArray['id'];
        return $this;

    }

    public function store()
    {
        $sqlQuery = "INSERT INTO users (username, password, email ) VALUES (? , ? , ?)";
        $dataArray = [$this->name, $this->pass, $this->email];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);


    }

    public function is_registered()
    {
        $query = "SELECT * FROM users WHERE  username='$this->name' AND password='$this->pass'";
        $sth = $this->dbh->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $status = $sth->fetchAll();

        $count = $sth->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function view()
    {
        $query = "SELECT id FROM users WHERE  username='$this->name'";
        $sth = $this->dbh->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $status = $sth->fetchAll();
        return $status;
    }



//    public function select($value)
//    {
//        $query = "SELECT * FROM review WHERE  user_id=".$value;
//        $sth = $this->dbh->query($query);
//        $sth->setFetchMode(PDO::FETCH_OBJ);
//        $status = $sth->fetchAll();
//        return $status;
//    }


    public function select($value)
    {
        $query = "SELECT * FROM user_preferences WHERE  user_id=" . $value;
        $sth = $this->dbh->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $status = $sth->fetchAll();
        return $status;
    }

    public function rs()
    {
        $query = "SELECT * FROM review";
        $sth = $this->dbh->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $status = $sth->fetchAll();
        return $status;
    }

    public function fetching_details($ha)
    {
//        $query = "SELECT * FROM user_preferences WHERE  user_id=".$value;
//        $sth = $this->dbh->query($query);
//        $sth->setFetchMode(PDO::FETCH_OBJ);
//        $status = $sth->fetchAll();
//        return $status;


        $query = "SELECT * FROM review WHERE  id=" . ($ha + 1);
        $sth = $this->dbh->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $status = $sth->fetchAll();
        return $status;

    }


    public function fetching_rsname($get_rsid)
    {


        $query = "SELECT * FROM real_estate WHERE  rs_id=" . ($get_rsid);
        $sth = $this->dbh->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $status = $sth->fetchAll();
        return $status;

    }

    public function show()
    {

        $sqlQuery = "SELECT * FROM rs_details WHERE rs_id=" . $this->id;


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData = $sth->fetch();

        return $oneData;

    }
}